# Contributor: Nick Black <dankamongmen@gmail.com>
# Maintainer: Nick Black <dankamongmen@gmail.com>
pkgname=notcurses
pkgver=2.3.17
pkgrel=0
pkgdesc="Blingful character graphics and TUI library"
url="https://nick-black.com/dankwiki/index.php/Notcurses"
arch="all"
license="Apache-2.0"
makedepends="cmake doctest-dev ffmpeg-dev libunistring-dev linux-headers ncurses-dev ncurses-terminfo readline-dev zlib-dev"
subpackages="$pkgname-dbg $pkgname-static $pkgname-dev $pkgname-doc
	$pkgname-libs $pkgname-demo ncneofetch ncls $pkgname-view $pkgname-tetris"
source="https://github.com/dankamongmen/notcurses/archive/v$pkgver/notcurses-$pkgver.tar.gz
	https://github.com/dankamongmen/notcurses/releases/download/v$pkgver/notcurses-doc-$pkgver.tar.gz
	"

# Don't run tests on mips64, builder is too slow.
[ "$CARCH" = "mips64" ] && options="!check"
[ "$CARCH" = "riscv64" ] && options="textrels"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DUSE_PANDOC=off \
		-DUSE_QRCODEGEN=off \
		$CMAKE_CROSSOPTS
	make -C build
}

check() {
	env TERM=vt100 CTEST_OUTPUT_ON_FAILURE=1 make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
	for i in 1 3 ; do
		find "$srcdir" -maxdepth 1 -type f -iname \*.$i -exec echo "$pkgdir"/usr/share/man/man$i {} \;
		find "$srcdir" -maxdepth 1 -type f -iname \*.$i -exec install -Dm644 -t "$pkgdir"/usr/share/man/man$i {} \;
	done
}

libs() {
	amove usr/lib/libnotcurses*.so.*
}

demo() {
	amove usr/bin/notcurses-demo
	amove usr/bin/notcurses-tester
	amove usr/bin/notcurses-info
	amove usr/share/notcurses
}

ncneofetch() {
	amove usr/bin/ncneofetch
}

ncls() {
	amove usr/bin/ncls
}

view() {
	amove usr/bin/ncplayer
}

tetris() {
	amove usr/bin/nctetris
}

sha512sums="
5fa2ef480e3ac8a2431a56f3af6dc3f94b4efd9c02ed903a1c59d1942f56e8e03d874649b16c365ecd05a355386720b9a6e0a952ce7d0d60d5b78aeceab89b46  notcurses-2.3.17.tar.gz
6e6896807d501b499464e1dcd3993a76932a794efe86f564c1bb532f6500412978dba1b46f18a5215afe07798521ff56c7e0ac951079cf3751b903d926399b3e  notcurses-doc-2.3.17.tar.gz
"
